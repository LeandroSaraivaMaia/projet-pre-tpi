﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpion_pp {
    //Holds the status of a game
    public class GameData {
        public delegate void DataGridChangeHandler();
        public DataGridChangeHandler dataGridChanged;
        public const string SAVE_DIRECTORY = "SAVES";
        public string[] players;
        public string lastWinningPlayer;
        public string IncarnatedPlayer => players[incarnatedPlayerIndex];
        public string actualPlayer => players[playerIndexThatNeedsToPlay];
        /*
         *  0 | 1 | 2
         * ---+---+---
         *  3 | 4 | 5
         * ---+---+---
         *  6 | 7 | 8
        */
        public static readonly int[] possibleCombinations = new int[] {
            //Horizontal
            0,1,2,  3,4,5,  6,7,8,
            //Vertical
            0,3,6, 1,4,7, 2,5,8,
            //Diagonal
            0,4,8, 2,4,6
        };
        /*Data of the game. Every value in this array correspond to a cell in the grid.
        If the value is -1, the cell is empty. If it is a number superior or equal to 0, it is a symbol
        that correspond to the index of the player*/
        public int[] dataGrid;
        public int playerIndexThatNeedsToPlay, incarnatedPlayerIndex;
        public bool isSoloGame;
        string[] IA_names = new string[] { "Robocop", "AlphaZero", "Marvin", "2B", "K2000", "Bender" };

        //Buffer value
        string dataToWrite, savePath, readData, dataBuffer;
        int iteratorBuffer;
        string[] readDataSplited;

        public GameData(string playerName) {//Used for singleplayer mode
            //Pick a random opponent name from an array
            string iaName = IA_names[new Random().Next(0, IA_names.Length)];
            this.players = new string[] { playerName, iaName };
            isSoloGame = true;
            InitializeEmptyGrid();
        }
        public GameData() {
            InitializeEmptyGrid();
        }

        void InitializeEmptyGrid() {
            dataGrid = Enumerable.Repeat(-1, GameBoard.NB_COLUMNS * GameBoard.NB_COLUMNS).ToArray();
        }

        public string[] GetOpponents() {
            List<string> opponentsList = this.players.ToList();
            opponentsList.RemoveAt(incarnatedPlayerIndex);
            return opponentsList.ToArray();

        }

        /// <summary>
        /// Save the current paused game. Saves the state of the game, the turn of the player and the name of the players.
        /// 
        /// A save file looks like this :
        /// 
        /// 1;
        /// playerName1,playerName2,playerNameN;
        /// -1,0,2,
        /// 1,2,0,
        /// -1,-1,0
        /// 
        /// Three infos are sperated by ";", first the index of the player that was playing, then the name of the players (ordered by index)
        /// and then the data of the grid. Note here that the new lines are only for convinience when a human read the file, the software don't need it.
        /// </summary>
        /// <param name="saveName">Name of the file</param>
        public void Save(string saveName) {
            savePath = Environment.CurrentDirectory + "\\" + SAVE_DIRECTORY + "\\";
            dataToWrite = "";

            //Add current player turn
            dataToWrite += playerIndexThatNeedsToPlay + ";" + Environment.NewLine;

            //Add player names
            foreach(string playerName in players) {
                dataToWrite += playerName + ",";
            }
            dataToWrite = dataToWrite.Remove(dataToWrite.Length-1);//Remove last character (the ',' followed by nothing)
            dataToWrite += ";" + Environment.NewLine;

            //Add grid data
            dataToWrite += GetDataGridInString();

            //Write file
            System.IO.Directory.CreateDirectory(savePath);//Be sure the directory exists
            System.IO.File.WriteAllText(savePath +  saveName + ".txt", dataToWrite);
        }

        /// <summary>
        /// Load game data from a file. Saves file contain the state of the game, the turn of the player and the name of the players.
        /// 
        /// A save file looks like this :
        /// 
        /// 1;
        /// playerName1,playerName2,playerNameN;
        /// -1,0,2,
        /// 1,2,0,
        /// -1,-1,0
        /// 
        /// Three infos are sperated by ";", first the index of the player that was playing, then the name of the players (ordered by index)
        /// and then the data of the grid. Note here that the new lines are only for convinience when a human read the file, the software don't need it.
        /// </summary>
        /// <param name="saveName">Name of the file</param>
        public void Load(string saveName) {
            savePath = Environment.CurrentDirectory + "\\" + SAVE_DIRECTORY + "\\";

            //Read file
            readData = System.IO.File.ReadAllText(savePath + saveName + ".txt");

            //Remove all carriage returns
            readData = readData.Replace(Environment.NewLine, "");
            
            readDataSplited = readData.Split(';');

            //Get current player turn
            playerIndexThatNeedsToPlay = int.Parse(readDataSplited[0]);

            //Get all players
            players = readDataSplited[1].Split(',');

            //Get grid data
            SetDataGridWithString(readDataSplited[2]);
        }

        /// <summary>
        /// Set the int array "dataGrid" from a string.
        /// The string looks like that :
        /// "0,-1,-1,0,-1,1,-1,1,0"
        /// 
        /// Every values represent a playerIndex (-1 is empty)
        /// </summary>
        /// <param name="data"></param>
        public void SetDataGridWithString(string data) {
            iteratorBuffer = 0;
            foreach (string gridVal in data.Split(',')) {
                dataGrid[iteratorBuffer] = int.Parse(gridVal);
                iteratorBuffer++;
            };
        }

        public string GetDataGridInString() {
            dataBuffer = "";
            for (int i = 0; i < dataGrid.Length; i += 3) {
                dataBuffer += dataGrid[i].ToString() + "," + dataGrid[i + 1].ToString() + "," + dataGrid[i + 2].ToString() + "," + Environment.NewLine;
            }
            dataBuffer = dataBuffer.Remove(dataBuffer.Length - 3);//Remove 2 last character (the ',' followed by nothing and the carriage return)
            return dataBuffer;
        }

        /// <summary>
        /// Selection of a cell by a player
        /// </summary>
        /// <param name="playerIndex"></param>
        /// <param name="cellIndex"></param>
        /// <returns>
        /// If the selection is valid, returns the index of the player that plays next.
        /// -1 means that the played index is already occupied
        /// -2 means that it is not to the selected player to play
        /// </returns>
        public int Play(int playerIndex, int cellIndex) {
            if (playerIndex != playerIndexThatNeedsToPlay) return -2;

            if (dataGrid[cellIndex] == -1) {
                dataGrid[cellIndex] = playerIndex;
                playerIndexThatNeedsToPlay = (playerIndexThatNeedsToPlay + 1 > players.Length-1) ? 0 : playerIndexThatNeedsToPlay + 1;
                return playerIndexThatNeedsToPlay;
            } else if (dataGrid[cellIndex] >= 0) {
                return -1;
            }

            throw new Exception("Unhandled exception");
        }

        /// <summary>
        /// Check who winns the morpion game.
        /// For each player that has at least a symbol in the grid, check if it wins.
        /// </summary>
        /// <returns>
        /// The id of the player that win
        /// If it returns -1 there is not winner
        /// If it returns -2, it is a tie game
        /// </returns>
        public int CheckWhoWins() {
            int[] playersIndex = dataGrid.Distinct().ToArray();

            foreach (int playerIndex in playersIndex) {
                if (playerIndex == -1) continue;//ignore empty cells
                for (int i = 0; i < possibleCombinations.Length; i += 3) {
                    if (dataGrid[possibleCombinations[i]] == playerIndex && dataGrid[possibleCombinations[i + 1]] == playerIndex && dataGrid[possibleCombinations[i + 2]] == playerIndex) {
                        lastWinningPlayer = players[playerIndex];
                        return playerIndex;
                    }
                }
            }

            //If there is no more empty cases in the grid, it is a tie game
            if (!dataGrid.Distinct().Contains(-1)) {
                return -2;
            }

            //No winner
            return -1;
        }

        public override string ToString() {
            return IncarnatedPlayer;
        }
    }
}

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using MonoGameUtilities;
using System.Linq;

namespace Morpion_pp
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MorpionPP : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        float dt;

        public MorpionPP() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Set values needed by UI elements
            UI_Element.defaultFrontColor = new Color(28, 28, 28);
            UI_Element.defaultBackgroundColor = new Color(244, 244, 244);
            UI_Element.defaultFont = Content.Load<SpriteFont>("consolas");
            UI_Element.singleCharacterSize = UI_Element.defaultFont.MeasureString("X");

            //Set values needed by the UI handler
            UI_Handler.mainSpriteBatch = spriteBatch;
            UI_Handler.screenWidth = graphics.PreferredBackBufferWidth;
            UI_Handler.screenHeight = graphics.PreferredBackBufferHeight;

            //Set images of symbols (cross & circle)
            GameBoard.circleTexture = Content.Load<Texture2D>("circle");
            GameBoard.crossTexture = Content.Load<Texture2D>("cross");


            //When building the tests executable, uncomment this line and go to "Project > properties > Application > Output type"
            //Set Output type to "Console Application" to build test executable and "Windows Application" to build normal executable
            Test.spriteBatch = spriteBatch; Test.Run(); return;

            //Load first screen of the game
            UI_Handler.LoadScreen(UI_Handler.main);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            dt = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            UI_Handler.Update(dt);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.SetRenderTarget(null);
            GraphicsDevice.Clear(Color.CornflowerBlue);

            UI_Handler.Draw();

            base.Draw(gameTime);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lidgren.Network;
using System.Net;

namespace Morpion_pp {
    public static class Network {
		public static bool isHosting => netServerHandler != null && netServerHandler.isRunning;
		public static NetworkHandler_Server netServerHandler;
		public static NetworkHandler_Client netClientHandler;
		
		public static string endReason, errorReason;

		public const int PORT = 14242, MAX_CONNECTIONS = 4;
	}
}

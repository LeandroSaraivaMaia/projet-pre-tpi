using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Morpion_pp {
	public class NetworkHandler_Client {
		public string serverHostName;
		bool isWaitingForGameToStart;
		string playerName, uniqueGameId = "0";
		NetClient netClient;
		NetPeerConfiguration netConfig;

		//Buffer values
		NetIncomingMessage message;
		string messageString;
		string[] messageData;

		public NetworkHandler_Client(string serverHostName, string playerName) {
			this.serverHostName = serverHostName;
			this.playerName = playerName;

			netConfig = new NetPeerConfiguration("Morpion++");
			netConfig.AutoFlushSendQueue = false;
			netClient = new NetClient(netConfig);
			netClient.RegisterReceivedCallback(new SendOrPostCallback(GotMessage));
			netClient.Start();
			netClient.Connect(serverHostName, Network.PORT, netClient.CreateMessage("This is an hail message"));
		}

		public void Shutdown() {
			netClient.Shutdown("Client shutted down");
		}

		void SendDataToServer(string data) {
			netClient.SendMessage(netClient.CreateMessage(data), NetDeliveryMethod.ReliableOrdered);
			Console.WriteLine("OUT\tCLIENT[Data]" + data);
			netClient.FlushSendQueue();
		}

		//Net actions
		public void Net_JoinGame() {
			SendDataToServer("joinGame;" + playerName);
			isWaitingForGameToStart = true;
		}
		public void Net_LeaveGame(string reason) {
			SendDataToServer("leaveGame;" + uniqueGameId + ";" + reason);
			isWaitingForGameToStart = false;
		}
		public void Net_PlayCell(int cellIndex) { SendDataToServer("playCell;" + uniqueGameId + ";" + UI_Handler.currentGame.incarnatedPlayerIndex + ";" + cellIndex.ToString()); }

		public void GotMessage(object peer) {
			while ((message = netClient.ReadMessage()) != null) {
				messageString = message.ReadString();
				Console.WriteLine("IN\tCLIENT[" + message.MessageType + "]" + messageString);

				if (message.MessageType == NetIncomingMessageType.Data) {
					messageData = messageString.Split(';');

					switch (messageData[0]) {
						case "startGame"://startGame;uniqueGameId;dataGrid;playerNames;playerIndexToPlay
							if (isWaitingForGameToStart) {
								uniqueGameId = messageData[1];

								UI_Handler.currentGame = new GameData();
								UI_Handler.currentGame.SetDataGridWithString(messageData[2]);
								UI_Handler.currentGame.players = messageData[3].Split(',');
								UI_Handler.currentGame.playerIndexThatNeedsToPlay = int.Parse(messageData[4]);

								//See what is the assigned player index from the players list
								for (int i = 0; i < UI_Handler.currentGame.players.Length; i++) {
									if (playerName == UI_Handler.currentGame.players[i]) {
										UI_Handler.currentGame.incarnatedPlayerIndex = i;
									}
								}

								UI_Handler.LoadScreen(UI_Handler.playing);

								isWaitingForGameToStart = false;
							}
							break;
						case "stopGame"://stopGame;uniqueGameId;reasonString
							if (messageData[1] == uniqueGameId) {
								Network.endReason = messageData[2];
								UI_Handler.LoadScreen(UI_Handler.networkEnd);
							}
							break;
						case "updateGame"://updateGame;uniqueGameId;gridData;currentPlayerIndex
							if (messageData[1] == uniqueGameId) {
								UI_Handler.currentGame.SetDataGridWithString(messageData[2]);

								UI_Handler.currentGame.playerIndexThatNeedsToPlay = int.Parse(messageData[3]);

								UI_Handler.currentGame.dataGridChanged();

								UI_Handler.LoadScreen(UI_Handler.playing);

							}
							break;
					}
				} else {
					if (message.MessageType == NetIncomingMessageType.StatusChanged) {
						if (messageString == "=Failed") {
							Network.errorReason = "Cannot connect to host";
							UI_Handler.LoadScreen(UI_Handler.netError);
						} else if (messageString == "Conn") {
							Net_JoinGame();
							UI_Handler.LoadScreen(UI_Handler.waitForGameToStart);
						}
					}
				}
				netClient.Recycle(message);
			}
		}
	}
}
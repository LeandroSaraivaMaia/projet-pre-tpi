﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Morpion_pp {
	public class NetworkHandler_Server {
		public static string endReason;
		public List<string> playersList;
		public bool isRunning => netServer.Status == NetPeerStatus.Running;
		string uniqueGameId;
		NetServer netServer;
		NetPeerConfiguration netConfig;

		//Buffer values
		NetIncomingMessage message;
		string messageString;
		string[] messageData;

		public NetworkHandler_Server(GameData gameData) {
			UI_Handler.currentGame = gameData;
			playersList = new List<string>();

			netConfig = new NetPeerConfiguration("Morpion++");
			netConfig.MaximumConnections = Network.MAX_CONNECTIONS;
			netConfig.Port = Network.PORT;
			netServer = new NetServer(netConfig);
			netServer.RegisterReceivedCallback(new SendOrPostCallback(GotMessage));
			netServer.Start();
		}

		public void Shutdown() {
			netServer.Shutdown("Shutdown server");
		}

		//Net actions
		public void Net_StartGame() {
			uniqueGameId = DateTime.Now.ToString("yyyyMMddHHmmssffff");

			UI_Handler.currentGame.players = playersList.ToArray();
			//new Random().Next() % 2 == 0 ? 1 : 0
			UI_Handler.currentGame.playerIndexThatNeedsToPlay = new Random().Next(0, UI_Handler.currentGame.players.Length); //Choose randomly what player needs to play

			SendDataToClients("startGame;" + uniqueGameId + ";" + string.Join(",", UI_Handler.currentGame.dataGrid) + ";" + string.Join(",", UI_Handler.currentGame.players) + ";" + UI_Handler.currentGame.playerIndexThatNeedsToPlay);

			UI_Handler.LoadScreen(UI_Handler.playing);
		}
		public void Net_StopGame(string reason) {
			SendDataToClients("stopGame;" + uniqueGameId + ";" + reason);
		}
		public void Net_UpdateGame() {
			SendDataToClients("updateGame;" + uniqueGameId + ";" + string.Join(",", UI_Handler.currentGame.dataGrid) + ";" + UI_Handler.currentGame.playerIndexThatNeedsToPlay);
		}

		public void SendDataToClients(string data) {
			netServer.SendToAll(netServer.CreateMessage(data), NetDeliveryMethod.ReliableOrdered);
			Console.WriteLine("OUT\tSERVER[Data]" + data);
			netServer.FlushSendQueue();
		}
		public void SendDataToClient(string data, NetConnection netConnection) {
			netServer.SendMessage(netServer.CreateMessage(data), netConnection, NetDeliveryMethod.ReliableOrdered);
			Console.WriteLine("OUT\tSERVER[Data]" + data);
			netServer.FlushSendQueue();
		}

		public void GotMessage(object peer) {
			while ((message = netServer.ReadMessage()) != null) {
				messageString = message.ReadString();
				Console.WriteLine("IN\tSERVER[" + message.MessageType + "]" + messageString);

				if (message.MessageType == NetIncomingMessageType.Data) {
					messageData = messageString.Split(';');

					switch (messageData[0]) {
						case "playCell"://playCell;uniqueGameID;playerIndex;cellIndex
							if (messageData[1] == uniqueGameId && messageData[2] == UI_Handler.currentGame.playerIndexThatNeedsToPlay.ToString()) {
								if (UI_Handler.currentGame.Play(int.Parse(messageData[2]), int.Parse(messageData[3])) >= 0) {
									Net_UpdateGame();
									UI_Handler.currentGame.dataGridChanged();

									switch (UI_Handler.currentGame.CheckWhoWins()) {
										case -1: /*No one wins*/UI_Handler.LoadScreen(UI_Handler.playing); break;
										case -2:
											Net_StopGame("Match nul");
											UI_Handler.LoadScreen(UI_Handler.tie);
											break;
										default:
											Net_StopGame("Le joueur " + UI_Handler.currentGame.lastWinningPlayer + " a gagné la partie !");
											UI_Handler.LoadScreen(UI_Handler.final);
											break;
									}
								}
							}
							break;
						case "joinGame"://joinGame;playerName

							//Ignore players that have the same name as another
							if (playersList.Contains(messageData[1])) {
								SendDataToClient("stopGame;0;Vous avez le même nom qu'un autre joueur", message.SenderConnection);
								break;
							}

							playersList.Add(messageData[1]);
							Net_StartGame();
							break;
						case "leaveGame"://leaveGame;uniqueGameId;reasonString
							if (messageData[1] == uniqueGameId) {
								Network.endReason = "A player left the game: " + messageData[2];
								UI_Handler.LoadScreen(UI_Handler.networkEnd);
							}
							break;
					}
				}
				netServer.Recycle(message);
			}
		}
	}
}

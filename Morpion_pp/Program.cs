﻿using System;

namespace Morpion_pp
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program {
        public static MorpionPP game;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            game = new MorpionPP();
            game.Run();
        }
    }
#endif
}
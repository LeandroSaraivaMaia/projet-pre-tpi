﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpion_pp {
    public static class Test {
        public static SpriteBatch spriteBatch;

        //Buffer values
        static bool testPassed;
        static string saveName;

        public static void Run() {
            RunListOfTests(UITests, "unit UI tests");
            RunListOfTests(gameLogicTests, "unit game logic tests");
            RunListOfTests(integrationTest, "integration test");
        }
        static void RunListOfTests(Dictionary<string, Func<bool>> testsToRun, string testListName) {
            ColoredWriteLine("[Starting " + testListName + "]", ConsoleColor.DarkYellow);

            foreach (KeyValuePair<string, Func<bool>> UITest in testsToRun) {
                Console.Write(UITest.Key);

                try {
                    testPassed = UITest.Value.Invoke();
                } catch(Exception exception) {
                    ColoredWriteLine("   EXCEPTION", ConsoleColor.DarkRed);
                    Console.WriteLine(exception);
                    continue;
                }

                if (testPassed) {
                    ColoredWriteLine("   PASS", ConsoleColor.DarkGreen);
                } else {
                    ColoredWriteLine("   FAIL", ConsoleColor.Red);
                }
            }

            ColoredWriteLine("[Finished " + testListName + "]", ConsoleColor.DarkYellow);
            Console.WriteLine();
        }
        static void ColoredWriteLine(string text, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ResetColor();
        }
        static MouseState InstanceMouseState(int x, int y, bool leftButtonPressed) => new MouseState(x, y, 0, leftButtonPressed ? ButtonState.Pressed : ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released);

        #region Help test functions
        static void ClickOnButton(string buttonName) {
            foreach (Button elem in UI_Handler.uiElements.OfType<Button>()) {
                if (elem.Text == buttonName) {
                    elem.onClick.Invoke();
                    return;
                }
            }

            throw new Exception("Button '" + buttonName + "' not found");
        }
        static void WriteInFirstTextBox(string textToWrite) {
            foreach (Textbox elem in UI_Handler.uiElements.OfType<Textbox>()) {
                elem.Text = textToWrite;
                return;
            }

            throw new Exception("No textbox found in this screen");
        }
        static void SelectItemInList(int selectIndex) {
            foreach (ItemList elem in UI_Handler.uiElements.OfType<ItemList>()) {
                elem.SelectedIndex = selectIndex;
                return;
            }

            throw new Exception("No itemList found in this screen");
        }
        static List<object> getFirstItemListData() {
            foreach (ItemList elem in UI_Handler.uiElements.OfType<ItemList>()) {
                return elem.Items;
            }

            throw new Exception("No itemList found in this screen");
        }
        #endregion

        #region tests
        static Dictionary<string, Func<bool>> UITests = new Dictionary<string, Func<bool>>() {
            { "Button tests", () => {
                Button btnTest = new Button(spriteBatch, new Rectangle(50, 50, 150, 50), "test btn");
                btnTest.onClick = () => {
                    btnTest.Text = "test btn: works";
                };
                UI_Handler.uiElements.Add(btnTest);

                //Emulate a click
                btnTest.onMouseLeftPressed.Invoke(InstanceMouseState(btnTest.WorldRect.X + btnTest.WorldRect.Width/2, btnTest.WorldRect.Y + btnTest.WorldRect.Height/2, true));
                btnTest.onMouseLeftReleased.Invoke(InstanceMouseState(btnTest.WorldRect.X + btnTest.WorldRect.Width/2, btnTest.WorldRect.Y + btnTest.WorldRect.Height/2, true));

                if (btnTest.Text != "test btn: works") return false;

                return true;
            } },
            { "TextBox tests", () => {
                Textbox txtTest = new Textbox(spriteBatch, new Rectangle(50, 150, 150, 30), "sample text");
                UI_Handler.uiElements.Add(txtTest);

                txtTest.Text += " that is used for tests purposes";
                txtTest.CursorIndex = txtTest.Text.Length;
                return true;
            } },
            { "ItemList tests", () => {
                ItemList lstTest = new ItemList(spriteBatch, new Rectangle(50, 200, 150, 180));
                UI_Handler.uiElements.Add(lstTest);

                for (int i = 0; i < 10; i++) {
                    lstTest.AddItem("test" + i);
                }

                if ((string)lstTest.SelectedItem != null) return false;
                lstTest.SelectedIndex = 0;
                if ((string)lstTest.SelectedItem != "test0") return false;
                lstTest.SelectedIndex = 3;
                if ((string)lstTest.SelectedItem != "test3") return false;

                lstTest.onMouseLeftPressed.Invoke(InstanceMouseState(lstTest.WorldRect.X + lstTest.WorldRect.Width/2, lstTest.WorldRect.Y + ItemList.ITEM_HEIGHT*2, true));
                if ((string)lstTest.SelectedItem != "test2") return false;

                return true;
            } },
            { "Label tests", () => {
                Label lblTest = new Label(spriteBatch, new Point(50, 400), "Sample label");
                UI_Handler.uiElements.Add(lblTest);

                int sizeBefore = lblTest.Rect.Width;

                lblTest.Text += " that is longer";

                if (sizeBefore >= lblTest.Rect.Width) return false;

                return true;
            } },
            { "Gameboard unit tests", () => {
                GameData testGameData = new GameData("testPlayer");
                GameBoard gamTest = new GameBoard(spriteBatch, new Point(400, 100), 100, testGameData);
                UI_Handler.uiElements.Add(gamTest);

                UI_Handler.currentGame = testGameData;

                bool gridChangeInvoked = false;
                gamTest.onGridChange = () => {
                    gridChangeInvoked = true;
                };

                //Click on (x:0,y:1)
                gamTest.onMouseLeftPressed.Invoke(InstanceMouseState(gamTest.WorldRect.X + gamTest.cellSize, gamTest.WorldRect.Y + gamTest.cellSize*2, true));

                if (!gridChangeInvoked) return false;
                if (testGameData.dataGrid[3] != 0) return false;

                return true;
            } },
        };

        static Dictionary<string, Func<bool>> gameLogicTests = new Dictionary<string, Func<bool>>() {
            { "Normal play", () => {
                GameData testGameData = new GameData();
                testGameData.players = new string[] { "player1", "player2" };
                testGameData.dataGrid = new int[] {
                     0,  1,  1,
                    -1,  0, -1,
                     0, -1, 1
                };
                testGameData.Play(0, 5);
                if (testGameData.CheckWhoWins() != -1) return false;

                return true;
            } },
            { "Win / Loose", () => {
                GameData testGameData = new GameData();
                testGameData.players = new string[] { "player1", "player2" };
                testGameData.dataGrid = new int[] {
                    -1,  1, -1,
                    -1,  0, -1,
                     0, -1, 1
                };
                testGameData.Play(0, 2);
                if (testGameData.CheckWhoWins() != 0) return false;

                return true;
            } },
            { "Tie", () => {
                GameData testGameData = new GameData();
                testGameData.players = new string[] { "player1", "player2" };
                testGameData.dataGrid = new int[] {
                     0,  1,  1,
                     1,  0,  0,
                     0, -1,  1
                };
                testGameData.Play(0, 7);
                if (testGameData.CheckWhoWins() != -2) return false;

                return true;
            } },
            { "Invalid play", () => {
                GameData testGameData = new GameData();
                testGameData.players = new string[] { "player1", "player2" };
                testGameData.dataGrid = new int[] {
                     0,  1,  0,
                     1, -1,  0,
                     1, -1,  1
                };
                if (testGameData.Play(0, 2) != -1) return false;
                if (testGameData.Play(1, 4) != -2) return false;

                return true;
            } }
        };

        static Dictionary<string, Func<bool>> integrationTest = new Dictionary<string, Func<bool>>() {
            { "Start a solo game", () => {
                UI_Handler.LoadScreen(UI_Handler.main);
                ClickOnButton("Nouvelle partie");
                WriteInFirstTextBox("testPlayer");
                ClickOnButton("Commencer la partie");
                return true;
            } },
            { "Play couple turns", () => {
                UI_Handler.currentGame.Play(0, 4);
                UI_Handler.currentGame.Play(1, 5);
                UI_Handler.currentGame.Play(0, 1);

                return true;
            } },
            { "Save a solo game", () => {
                ClickOnButton("Menu pause");
                ClickOnButton("Sauvegarder la partie en cours");
                saveName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "testSave";
                WriteInFirstTextBox(saveName);
                ClickOnButton("Sauvegarder");
                ClickOnButton("Retour au menu pause");
                return true;
            } },
            { "Load a solo game", () => {
                ClickOnButton("Retour au menu principal");
                ClickOnButton("Charger une partie");

                //Select correct save
                List<object> items = getFirstItemListData();
                for (int i = 0; i < items.Count(); i++) {
                    if ((string)items[i] == saveName) {
                        SelectItemInList(i);
                    }
                }

                ClickOnButton("Charger cette partie");

                return true;
            } },
            { "Finish a solo game", () => {
                //Did the tests to late, no time to finish...
                return true;
            } },
        };
        #endregion
    }
}

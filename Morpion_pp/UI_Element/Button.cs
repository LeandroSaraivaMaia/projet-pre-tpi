using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;


namespace Morpion_pp {
    class Button : UI_Element {
        public static Color hoverColor = new Color(150, 150, 150, 200), pressedColor = new Color(80, 80, 80, 200);
        public Action onClick;
        Texture2D backgroundTexture;
        string text;
        protected bool hasBeenPressed;

        //Buffer values
        Vector2 textPosition;

        //When the text or the position of the UI_Element is changed, the text position is recalculated
        public string Text {
            get { return text; }
            set { text = value; CalculateRenderTarget(); }
        }

        public Button(SpriteBatch mainSpriteBatch, Rectangle rect, string text, ECenterMode centerMode = ECenterMode.left) : base(mainSpriteBatch, centerMode) {
            this.rect = rect;
            this.text = text;

            backgroundTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height, defaultBackgroundColor);
            renderTarget = new RenderTarget2D(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height);

            onClick = () => { };

            //When mouse enter / leave / pressed
            onMouseEnter = (MouseState mouseState) => { hasBeenPressed = false; CalculateRenderTarget(); };
            onMouseLeave = (MouseState mouseState) => { hasBeenPressed = false; CalculateRenderTarget(); };
            onMouseLeftPressed = (MouseState mouseState) => { hasBeenPressed = true; CalculateRenderTarget(); };
            onMouseLeftReleased = (MouseState MouseState) => { if (hasBeenPressed) onClick.Invoke(); hasBeenPressed = false; CalculateRenderTarget(); };

            CalculateWorldRect();
            CalculateRenderTarget();
        }

        protected override void CalculateRenderTarget() {
            mainSpriteBatch.GraphicsDevice.SetRenderTarget(renderTarget);
            mainSpriteBatch.GraphicsDevice.Clear(Color.CornflowerBlue);
            mainSpriteBatch.Begin();
            mainSpriteBatch.Draw(backgroundTexture, localRect, Color.White);
            if (isMouseInside) {
                if (hasBeenPressed) { //Pressed
                    mainSpriteBatch.Draw(backgroundTexture, localRect, pressedColor);
                } else { //Hovered
                    mainSpriteBatch.Draw(backgroundTexture, localRect, hoverColor);
                }
            }
            textPosition = new Vector2(
                worldRect.Width / 2 - defaultFont.MeasureString(text).X / 2,
                worldRect.Height / 2 - defaultFont.MeasureString(text).Y / 3
            );
            mainSpriteBatch.DrawString(defaultFont, text, textPosition, defaultFrontColor);
            mainSpriteBatch.End();
        }

        public override void Update(float dt) { }

        public override void Draw(SpriteBatch temp) {
            base.Draw(mainSpriteBatch);

            mainSpriteBatch.Draw(renderTarget, worldRect, Color.White);
        }
    }
}

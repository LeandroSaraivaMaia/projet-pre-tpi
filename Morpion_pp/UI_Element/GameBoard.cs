﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Morpion_pp {
    class GameBoard : UI_Element {
        public const int NB_COLUMNS = 3;
        public static Texture2D crossTexture, circleTexture;
        public Action onGridChange;

        Texture2D backgroundTexture, borderTexture, gridTexture;
        GameData gameData;
        public int size => rect.Width;
        public int cellSize => size / NB_COLUMNS;
        int coordToIndex(int x, int y) => NB_COLUMNS * y + x;
        Point indexToCoord(int index) => new Point(index % NB_COLUMNS, index / NB_COLUMNS);

        //When the text or the position of the UI_Element is changed, the text position is recalculated
        public Rectangle Rect {
            get { return rect; }
            set { rect = value; CalculateRenderTarget(); }
        }

        public GameBoard(SpriteBatch mainSpriteBatch, Point position, int size, GameData gameData, ECenterMode centerMode = ECenterMode.left) : base(mainSpriteBatch, centerMode) {
            this.rect = new Rectangle(position.X, position.Y, size, size);
            this.gameData = gameData;
            this.gameData.dataGridChanged += () => {
                CalculateRenderTarget();
            };

            onMouseLeftPressed = leftMousePressed;
            onGridChange = () => { };

            backgroundTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height, defaultBackgroundColor);
            borderTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height, defaultFrontColor, 1);
            gridTexture = Tools.CreateGridTexture(mainSpriteBatch.GraphicsDevice, NB_COLUMNS, NB_COLUMNS, cellSize + 1, defaultFrontColor);

            renderTarget = new RenderTarget2D(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height);
            CalculateWorldRect();
            CalculateRenderTarget();
        }

        public override void Update(float dt) { }

        void leftMousePressed(MouseState mouseState) {
            int x = (mouseState.X - worldRect.X) / (cellSize + 1);
            int y = (mouseState.Y - worldRect.Y) / (cellSize + 1);
            int selectedCellIndex = coordToIndex(x, y);

            if (gameData.Play(gameData.incarnatedPlayerIndex, selectedCellIndex) >= 0) {

                if (!gameData.isSoloGame) {
                    if (Network.isHosting) {
                        Network.netServerHandler.Net_UpdateGame();
                        switch (gameData.CheckWhoWins()) {
                            case -1: /*No one wins*/break;
                            case -2:
                                UI_Handler.LoadScreen(UI_Handler.tie);
                                Network.netServerHandler.Net_StopGame("Match nul");
                                break;
                            default:
                                UI_Handler.LoadScreen(UI_Handler.final);
                                Network.netServerHandler.Net_StopGame("Le joueur " + UI_Handler.currentGame.lastWinningPlayer + " a gagné la partie !");
                                break;
                        }
                    } else {
                        Network.netClientHandler.Net_PlayCell(selectedCellIndex);
                    }
                }

                switch (gameData.CheckWhoWins()) {
                    case -1: /*No one wins*/break;
                    case -2: UI_Handler.LoadScreen(UI_Handler.tie); return;
                    default: UI_Handler.LoadScreen(UI_Handler.final); return;
                }

                CalculateRenderTarget();
                onGridChange.Invoke();

                if (gameData.isSoloGame) {//IA plays
                    List<int> playableCells = new List<int>();

                    for (int i = 0; i < gameData.dataGrid.Length; i++) {
                        if (gameData.dataGrid[i] == -1) {
                            playableCells.Add(i);
                        }
                    }

                    gameData.Play(1, playableCells[new Random().Next(playableCells.Count())]);
                    if (gameData.CheckWhoWins() != -1) {
                        UI_Handler.LoadScreen(UI_Handler.final);
                        return;
                    }

                    switch (gameData.CheckWhoWins()) {
                        case -1: /*No one wins*/break;
                        case -2: UI_Handler.LoadScreen(UI_Handler.tie); return;
                        default: UI_Handler.LoadScreen(UI_Handler.final); return;
                    }

                    CalculateRenderTarget();
                    onGridChange.Invoke();
                }
            }
        }

        protected override void CalculateRenderTarget() {
            mainSpriteBatch.GraphicsDevice.SetRenderTarget(renderTarget);
            mainSpriteBatch.GraphicsDevice.Clear(Color.CornflowerBlue);
            mainSpriteBatch.Begin();
            mainSpriteBatch.Draw(backgroundTexture, Vector2.Zero, Color.White);
            mainSpriteBatch.Draw(gridTexture, Vector2.Zero, Color.White);
            mainSpriteBatch.Draw(borderTexture, Vector2.Zero, Color.White);
            for (int i = 0; i < gameData.dataGrid.Length; i++) {
                Vector2 texturePosition = new Vector2(i % NB_COLUMNS * cellSize + cellSize / 2, i / NB_COLUMNS * cellSize + cellSize / 2);
                switch (gameData.dataGrid[i]) {
                    case 0: mainSpriteBatch.Draw(texture: circleTexture, position: texturePosition, color: Color.White, scale: new Vector2((float)cellSize /crossTexture.Width), origin: new Vector2(circleTexture.Width/2, circleTexture.Height/2)); break;
                    case 1: mainSpriteBatch.Draw(texture: crossTexture, position: texturePosition, color: Color.White, scale: new Vector2((float)cellSize / crossTexture.Width), origin: new Vector2(crossTexture.Width / 2, circleTexture.Height / 2)); break;
                }
            }
            mainSpriteBatch.End();
        }

        public override void Draw(SpriteBatch temp) {
            base.Draw(mainSpriteBatch);

            mainSpriteBatch.Draw(renderTarget, worldRect, Color.White);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;


namespace Morpion_pp {
    class ItemList : UI_Element {
        public const int ITEM_HEIGHT = 50;
        public const int SCROLLBAR_WIDTH = 5;
        public static readonly Color SCROLLBAR_COLOR = new Color(0.6f, 0.6f, 0.6f, 0.9f);

        int selectedIndex = -1;
        float scrollOffset, scrollSpeed = 0.005f;
        Texture2D backgroundTexture, borderTexture, selectedItemTexture, scrollBarTexture;
        MouseState mouseState, oldMouseState;
        List<object> items;

        //Buffer variables
        float mouseWheelDelta;

        //When the text or the position of the UI_Element is changed, the text position is recalculated
        public List<object> Items {
            get { return items; }
        }
        public void AddItem(object Item) {
            items.Add(Item);
            CalculateScrollBarTexture();
            CalculateRenderTarget();
        }
        public void RemoveItemAt(int index) {
            items.RemoveAt(index);
            if (selectedIndex == index) selectedIndex = -1;
            CalculateScrollBarTexture();
            CalculateRenderTarget();
        }

        public object SelectedItem { get {
                if (selectedIndex >= 0 && selectedIndex < items.Count) {
                    return items[selectedIndex];
                } else {
                    return null;
                }
            }
        }
        public int SelectedIndex {
            get { return selectedIndex; }
            set { selectedIndex = value; CalculateRenderTarget(); }
        }

        public ItemList(SpriteBatch mainSpriteBatch, Rectangle rect, List<object> items = null, ECenterMode centerMode = ECenterMode.left) : base(mainSpriteBatch, centerMode) {
            this.rect = rect;
            this.items = items == null ? new List<object>() : items;
            CalculateScrollBarTexture();

            backgroundTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height, defaultBackgroundColor);
            borderTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, rect.Width, ITEM_HEIGHT, defaultFrontColor, 1);
            selectedItemTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, rect.Width, ITEM_HEIGHT, new Color(180, 180, 255, 255));

            onMouseLeftPressed = (MouseState mouseState) => {
                selectedIndex = (int)(mouseState.Y - worldRect.Y - scrollOffset) / ITEM_HEIGHT;
                CalculateRenderTarget();
            };

            renderTarget = new RenderTarget2D(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height);
            CalculateWorldRect();
            CalculateRenderTarget();
        }

        public override void Update(float dt) {
            mouseState = Mouse.GetState();

            if (isMouseInside) {
                mouseWheelDelta = mouseState.ScrollWheelValue - oldMouseState.ScrollWheelValue;
                if (mouseWheelDelta != 0) {
                    scrollOffset += mouseWheelDelta * scrollSpeed * dt;

                    CalculateRenderTarget();
                }
            }

            oldMouseState = mouseState;
        }

        int GetScrollBarHeight() => items.Count * ITEM_HEIGHT > rect.Height ? (rect.Height * rect.Height) / (items.Count * ITEM_HEIGHT) : rect.Height;
        void CalculateScrollBarTexture() {
            if (scrollBarTexture != null) {
                if (scrollBarTexture.Height != GetScrollBarHeight()) {
                    scrollBarTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, SCROLLBAR_WIDTH, GetScrollBarHeight(), SCROLLBAR_COLOR);
                }
            } else {
                scrollBarTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, SCROLLBAR_WIDTH, GetScrollBarHeight(), SCROLLBAR_COLOR);
            }
        }
        protected override void CalculateRenderTarget() {
            //Clamp textOffset value
            float max = rect.Height > items.Count * ITEM_HEIGHT ? rect.Height : items.Count * ITEM_HEIGHT - rect.Height;
            scrollOffset = MathHelper.Clamp(scrollOffset, -(items.Count * ITEM_HEIGHT - rect.Height), 0);

            mainSpriteBatch.GraphicsDevice.SetRenderTarget(renderTarget);
            mainSpriteBatch.GraphicsDevice.Clear(Color.CornflowerBlue);
            mainSpriteBatch.Begin();
            mainSpriteBatch.Draw(backgroundTexture, Vector2.Zero, Color.White);
            for (int i = 0; i < items.Count; i++) {
                if (i == selectedIndex) {
                    mainSpriteBatch.Draw(selectedItemTexture, new Vector2(0, scrollOffset + ITEM_HEIGHT * i), Color.White);
                }
                mainSpriteBatch.DrawString(defaultFont, items[i].ToString(), new Vector2(0, scrollOffset + ITEM_HEIGHT * i + ITEM_HEIGHT / 2 - defaultFont.MeasureString(items[i].ToString()).Y / 3), defaultFrontColor);
                mainSpriteBatch.Draw(borderTexture, new Vector2(0, scrollOffset + ITEM_HEIGHT * i), Color.White);
            }
            if (rect.Height != scrollBarTexture.Height) {
                mainSpriteBatch.Draw(scrollBarTexture, new Vector2(rect.Width - SCROLLBAR_WIDTH, -scrollOffset * rect.Height / (items.Count * ITEM_HEIGHT)), Color.White);
            }
            mainSpriteBatch.End();
        }

        public override void Draw(SpriteBatch temp) {
            base.Draw(mainSpriteBatch);

            mainSpriteBatch.Draw(renderTarget, worldRect, Color.White);
        }
    }
}

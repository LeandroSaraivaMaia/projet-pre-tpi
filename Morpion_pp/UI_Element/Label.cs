﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpion_pp {
    class Label : UI_Element {
        string text;
        Point textDimensions;//Width, Height of the string

        //When the text or the position of the UI_Element is changed, the text position is recalculated
        public string Text {
            get { return text; }
            set {
                text = value;
                textDimensions = defaultFont.MeasureString(text).ToPoint();
                Rect = new Rectangle(rect.X, rect.Y, textDimensions.X, textDimensions.Y);
            }
        }

        protected override void CalculateRenderTarget() { /*Not used for this UI element*/ }

        protected override void CalculateWorldRect() {
            switch (centerMode) {
                case ECenterMode.left: worldRect = rect; break;
                case ECenterMode.right: worldRect = new Rectangle(rect.X - rect.Width, rect.Y, rect.Width, rect.Height); break;
                case ECenterMode.middle: worldRect = new Rectangle(rect.X - rect.Width / 2, rect.Y, rect.Width, rect.Height); break;
                default: throw new NotImplementedException();
            }
            localRect = new Rectangle(0, 0, worldRect.Width, worldRect.Height);
        }

        public Label(SpriteBatch mainSpriteBatch, Point position, string text, ECenterMode centerMode = ECenterMode.left) : base (mainSpriteBatch, centerMode) {
            this.Rect = new Rectangle(position.X, position.Y, 0, 0);
            this.Text = text;
        }

        public override void Update(float dt) { }

        public override void Draw(SpriteBatch spriteBatch) {
            base.Draw(spriteBatch);

            spriteBatch.DrawString(defaultFont, text, worldRect.Location.ToVector2(), defaultFrontColor);
        }
    }
}

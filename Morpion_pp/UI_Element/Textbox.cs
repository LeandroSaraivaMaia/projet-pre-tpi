﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoGameUtilities;

namespace Morpion_pp {
    class Textbox : UI_Element {
        public static int lettersRangeMin = 65, lettersRangeMax = 91, decimalRangeMin = 48, decimalRangeMax = 57/*58*/,
        keyPadDecimalRangeMin = 96, keyPadDecimalRangeMax = 105;
        Vector2 textPosition, cursorPosition;
        MouseState mouseState, oldMouseState;
        KeyboardState kbState;
        string text;
        Texture2D borderTexture, backgroundTexture, cursorTexture;
        int cursorIndex = 0;
        float textOffset;
        bool[] pressedKeys, oldPressedKeys;
        bool isSelected, isShiftDown;

        //Buffer values (Variable reused in multiple place for a brief moment. Declared here so we don't reallow a new variable each time)
        int intKey, oldCursorIndex;
        float localCursorPosition;

        //When the text or the position of the UI_Element is changed, the text position is recalculated
        public string Text {
            get { return text; }
            set { text = value; CalculateRenderTarget(); }
        }
        public int CursorIndex {
            get { return cursorIndex; }
            set {
                oldCursorIndex = cursorIndex;
                cursorIndex = value;
                localCursorPosition = defaultFont.MeasureString(text.Substring(0, cursorIndex)).X;

                if (cursorIndex > oldCursorIndex) {//If the cursor move to the right
                    if (localCursorPosition > textOffset + rect.Width) {
                        textOffset = localCursorPosition - rect.Width;
                    }
                } else if (cursorIndex < oldCursorIndex) {//If the cursor move to the left
                    if (textOffset > localCursorPosition) {
                        textOffset = localCursorPosition;
                    }
                }
                CalculateRenderTarget();
            }
        }
        
        public Textbox(SpriteBatch mainSpriteBatch, Rectangle rect, string text, ECenterMode centerMode = ECenterMode.left) : base(mainSpriteBatch, centerMode) {
            this.text = text;
            this.rect = rect;

            cursorTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, 2, (int)singleCharacterSize.Y, defaultFrontColor);
            backgroundTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height, defaultBackgroundColor);
            borderTexture = Tools.CreateRectTexture(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height, Color.Gray, 1);

            //The key code we are interested in range from 0 to 190 (190='.')
            pressedKeys = new bool[191];
            oldPressedKeys = new bool[191];

            renderTarget = new RenderTarget2D(mainSpriteBatch.GraphicsDevice, rect.Width, rect.Height);
            CalculateWorldRect();
            CalculateRenderTarget();
        }

        /// <summary>
        /// Handle pressed keys. The key was up the frame before
        /// </summary>
        /// <param name="pressedKey">The pressed key</param>
        void KeyPressed(Keys pressedKey) {
            if (!isSelected) return;
            intKey = (int)pressedKey;

            //Pressed key is in the range of the letters (A-Z)
            if (intKey >= lettersRangeMin && intKey <= lettersRangeMax) {
                text = text.Insert(CursorIndex, isShiftDown ? pressedKey.ToString() : pressedKey.ToString().ToLower());
                CursorIndex++;
                return;
            }

            //Pressed key is in the range of the decimals (0-9)
            if (intKey >= decimalRangeMin && intKey <= decimalRangeMax) {
                text = text.Insert(CursorIndex, (intKey - decimalRangeMin).ToString());
                CursorIndex++;
                return;
            }

            //Pressed key is in the range of the decimals in the keypad (0-9)
            if (intKey >= keyPadDecimalRangeMin && intKey <= keyPadDecimalRangeMax) {
                text = text.Insert(CursorIndex, (intKey - keyPadDecimalRangeMin).ToString());
                CursorIndex++;
                return;
            }

            //Accept the '.' character (for entering ip addresses)
            if (pressedKey == Keys.OemPeriod) {
                text = text.Insert(CursorIndex, ".");
                CursorIndex++;
                return;
            }

            //Accept the '-' character (for some hostnames)
            if (pressedKey == Keys.OemMinus) {
                text = text.Insert(CursorIndex, "-");
                CursorIndex++;
                return;
            }

            //Other keys
            switch (pressedKey) {
                case Keys.Left: if (CursorIndex > 0) CursorIndex--; break;
                case Keys.Right: if (CursorIndex < text.Length) CursorIndex++; break;
                case Keys.Delete: if (CursorIndex < text.Length) text = text.Remove(CursorIndex, 1); CalculateRenderTarget(); break;
                case Keys.Home: CursorIndex = 0; break;
                case Keys.End: CursorIndex = text.Length; break;
                case Keys.Back:
                    if (CursorIndex > 0) {
                        text = text.Remove(CursorIndex - 1, 1);
                        CursorIndex--;
                    }
                    break;
                case Keys.Space:
                    text = text.Insert(CursorIndex, " ");
                    CursorIndex++;
                    break;
            }
        }

        /// <summary>
        /// Handle the release of the key. The key was down the frame before
        /// </summary>
        /// <param name="releasedKey">The released key</param>
        void KeyReleased(Keys releasedKey) { }

        public override void Update(float dt) {
            kbState = Keyboard.GetState();
            mouseState = Mouse.GetState();
            isShiftDown = kbState.IsKeyDown(Keys.LeftShift) || kbState.IsKeyDown(Keys.RightShift);
            pressedKeys = new bool[pressedKeys.Length]; //Reset pressedKeys array so all values are back to false

            //We iterate throught every key that are pressed down during this frame
            foreach (Keys key in kbState.GetPressedKeys()) {
                intKey = (int)key;

                //If there is a pressed key that is not in our range of interested keys, we ignore it
                if (intKey > pressedKeys.Length || intKey < 0) continue;

                //If a key was up the frame before, we raise the event that handle pressed keys
                if (!oldPressedKeys[intKey]) {
                    KeyPressed(key);
                } else {
                    oldPressedKeys[intKey] = false;
                }
                pressedKeys[intKey] = true;
            }

            for (int i = 0; i < oldPressedKeys.Length; i++) {
                // If a key was pressed the frame before and was not set to false during the iteration of the
                // pressed keys (meaning that this key is not pressed anymore), we raise the event that handle released keys 
                if (oldPressedKeys[i]) {
                    KeyReleased((Keys)i);
                }
            }

            if (mouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released) {
                if (mouseState.X >= worldRect.X && mouseState.Y >= worldRect.Y && mouseState.X <= worldRect.X + rect.Width && mouseState.Y <= worldRect.Y + rect.Height) {
                    if (!isSelected) {
                        isSelected = true;
                        CalculateRenderTarget();
                    }
                } else {
                    if (isSelected) {
                        isSelected = false;
                        CalculateRenderTarget();
                    }
                }
            }

            oldPressedKeys = pressedKeys;
            oldMouseState = mouseState;
        }

        protected override void CalculateRenderTarget() {
            textPosition = new Vector2(-textOffset, worldRect.Height / 2 - defaultFont.MeasureString(text).Y / 3);
            cursorPosition = new Vector2(
                -textOffset + defaultFont.MeasureString(text.Substring(0, CursorIndex)).X,
                worldRect.Height / 2 - cursorTexture.Height / 2
            );

            mainSpriteBatch.GraphicsDevice.SetRenderTarget(renderTarget);
            mainSpriteBatch.GraphicsDevice.Clear(Color.CornflowerBlue);
            mainSpriteBatch.Begin();
            mainSpriteBatch.Draw(backgroundTexture, Vector2.Zero, Color.White);
            mainSpriteBatch.Draw(borderTexture, Vector2.Zero, isSelected ? Color.Black : Color.White);
            mainSpriteBatch.DrawString(defaultFont, text, textPosition, defaultFrontColor);
            if (isSelected) {
                mainSpriteBatch.Draw(cursorTexture, cursorPosition, Color.White);
            }
            mainSpriteBatch.End();
        }

        public override void Draw(SpriteBatch toBeRemoved) {
            base.Draw(mainSpriteBatch);


            mainSpriteBatch.Draw(renderTarget, worldRect, Color.White);
        }
    }
}

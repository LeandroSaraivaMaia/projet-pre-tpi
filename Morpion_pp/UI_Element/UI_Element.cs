using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpion_pp {
    public abstract class UI_Element {
        public enum ECenterMode { left, right, middle };
        public delegate void UI_MouseEvent(MouseState mouseState);
        public static SpriteFont defaultFont;
        public static Color defaultFrontColor, defaultBackgroundColor;
        public static Vector2 singleCharacterSize;

        public bool isVisible = true;
        public bool isMouseInside, isMouseLeftPressed;
        public bool oldIsMouseInside, oldIsMouseLeftPressed; //Values with "old" represent the state during the last frame
        public UI_MouseEvent onMouseLeftPressed, onMouseLeftReleased, onMouseEnter, onMouseLeave, onMouseWheel;
        public RenderTarget2D renderTarget;
        public SpriteBatch mainSpriteBatch; //Holds a reference to the main spritebatch
        protected Rectangle rect, worldRect, localRect;
        protected ECenterMode centerMode;

        public Rectangle Rect {
            get { return rect; }
            set {
                rect = value;
                CalculateWorldRect();
                CalculateRenderTarget();
            }
        }
        public Rectangle WorldRect {
            get { return worldRect; }
            set { worldRect = value; }
        }
        public ECenterMode CenterMode {
            get { return centerMode; }
            set {
                if (centerMode == value) return;
                centerMode = value;

                CalculateWorldRect();
                CalculateRenderTarget();
            }
        }
        protected virtual void CalculateWorldRect() {
            switch (centerMode) {
                case ECenterMode.left: worldRect = rect; break;
                case ECenterMode.right: worldRect = new Rectangle(rect.X - rect.Width, rect.Y, rect.Width, rect.Height); break;
                case ECenterMode.middle: worldRect = new Rectangle(rect.X - rect.Width / 2, rect.Y, rect.Width, rect.Height); break;
                default: throw new NotImplementedException();
            }
            localRect = new Rectangle(0, 0, worldRect.Width, worldRect.Height);
        }

        /// <summary>
        /// Generic constructor for UI elements
        /// </summary>
        /// <param name="centerMode">Center mode of the element</param>
        public UI_Element(SpriteBatch mainSpriteBatch, ECenterMode centerMode = ECenterMode.left) {
            this.mainSpriteBatch = mainSpriteBatch;
            this.centerMode = centerMode;

            //Instanciate every events
            onMouseLeftPressed = (MouseState mouseState) => { };
            onMouseLeftReleased = (MouseState mouseState) => { };
            onMouseEnter = (MouseState mouseState) => { };
            onMouseLeave = (MouseState mouseState) => { };
            onMouseWheel = (MouseState mouseState) => { };
        }
        
        /// <summary>
        /// Called once per frame, this is used to manipulate this object during runtime.
        /// </summary>
        /// <param name="dt">dt stands for Delta Time. Number of miliseconds between each frame.</param>
        public abstract void Update(float dt);

        /// <summary>
        /// Called once per frame, this is used to draw object into the screen.
        /// </summary>
        public virtual void Draw(SpriteBatch sb) {
            if (!isVisible) return;
        }

        /// <summary>
        /// Calculate the graphical representation of the element. According to center mode
        /// </summary>
        protected abstract void CalculateRenderTarget();
    }
}

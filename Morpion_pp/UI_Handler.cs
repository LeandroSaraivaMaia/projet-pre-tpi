﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MonoGameUtilities;

namespace Morpion_pp {
    public static class UI_Handler {
        public static List<UI_Element> uiElements = new List<UI_Element>();
        public static SpriteBatch mainSpriteBatch;
        public static int screenWidth, screenHeight;
        public static int halfScreenWidth => screenWidth / 2;
        public static int halfScreenHeight => screenHeight / 2;
        public static GameData currentGame;
        static MouseState mouseState, oldMouseState;

        //Buffer values
        static string[] saveFiles;
        static string savePath;

        static void CleanScreen() {
            uiElements = new List<UI_Element>();
        }
        public static void LoadScreen(Action uiElementsInitialization) {
            CleanScreen();
            uiElementsInitialization.Invoke();
        }
        public static void Update(float dt) {
            mouseState = Mouse.GetState();
            foreach (UI_Element elem in uiElements) {
                elem.isMouseInside = Tools.Pos2Rect(mouseState.X, mouseState.Y, elem.WorldRect.X, elem.WorldRect.Y, elem.WorldRect.Width, elem.WorldRect.Height);
                elem.isMouseLeftPressed = mouseState.LeftButton == ButtonState.Pressed;

                if (elem.isMouseInside && !elem.oldIsMouseInside) {
                    elem.onMouseEnter.Invoke(mouseState);
                }
                if (!elem.isMouseInside && elem.oldIsMouseInside) {
                    elem.onMouseLeave.Invoke(mouseState);
                }

                if (elem.isMouseInside && elem.isMouseLeftPressed && !elem.oldIsMouseLeftPressed) {
                    elem.onMouseLeftPressed.Invoke(mouseState);
                }
                if (elem.isMouseInside && !elem.isMouseLeftPressed && elem.oldIsMouseLeftPressed) {
                    elem.onMouseLeftReleased.Invoke(mouseState);
                }

                if (elem.isMouseInside && mouseState.ScrollWheelValue != oldMouseState.ScrollWheelValue) {
                    elem.onMouseWheel.Invoke(mouseState);
                }

                elem.Update(dt);

                elem.oldIsMouseInside = elem.isMouseInside;
                elem.oldIsMouseLeftPressed = elem.isMouseLeftPressed;
            }
            oldMouseState = mouseState;
        }
        public static void Draw() {
            mainSpriteBatch.Begin();
            foreach (UI_Element elem in uiElements) {
                elem.Draw(mainSpriteBatch);
            }
            mainSpriteBatch.End();
        }
        public static string[] GetSaveFiles() {
            //Get all files from save path
            saveFiles = System.IO.Directory.GetFiles(Environment.CurrentDirectory + "\\" + GameData.SAVE_DIRECTORY, "*.txt");

            for (int i = 0; i < saveFiles.Length; i++) {
                saveFiles[i] = saveFiles[i].Split('\\').Last(); //Get only the file name (not the full path)
                saveFiles[i] = saveFiles[i].Remove(saveFiles[i].Length - ".txt".Length); //Remove .txt from files
            }

            return saveFiles;
        }

        #region All screens definition
        public static Action main = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 40), "Menu principal", UI_Element.ECenterMode.middle));

            Button btnNewGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 140, 400, 50), "Nouvelle partie", UI_Element.ECenterMode.middle);
            btnNewGame.onClick = () => { LoadScreen(newGame); };
            uiElements.Add(btnNewGame);

            Button btnLoadGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 210, 400, 50), "Charger une partie", UI_Element.ECenterMode.middle);
            btnLoadGame.onClick = () => { LoadScreen(loadGame); };
            uiElements.Add(btnLoadGame);

            Button btnHostGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 280, 400, 50), "Héberger une partie en ligne", UI_Element.ECenterMode.middle);
            btnHostGame.onClick = () => { LoadScreen(hostOnlineGame); };
            uiElements.Add(btnHostGame);

            Button btnJoinGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 350, 400, 50), "Rejoindre une partie en ligne", UI_Element.ECenterMode.middle);
            btnJoinGame.onClick = () => { LoadScreen(joinOnlineGame); };
            uiElements.Add(btnJoinGame);

            Button btnQuitGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 420, 400, 50), "Quitter le jeu", UI_Element.ECenterMode.middle);
            btnQuitGame.onClick = () => { Program.game.Exit(); };
            uiElements.Add(btnQuitGame);
        };
        public static Action newGame = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 40), "Nouvelle partie", UI_Element.ECenterMode.middle));

            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 150), "Entrez votre nom :", UI_Element.ECenterMode.middle));

            Textbox txtName = new Textbox(mainSpriteBatch, new Rectangle(halfScreenWidth, 180, 150, 40), "", UI_Element.ECenterMode.middle);
            uiElements.Add(txtName);

            Button btnStartGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 300, 220, 50), "Commencer la partie", UI_Element.ECenterMode.middle);
            btnStartGame.onClick = () => {
                if (txtName.Text.Length > 0) {
                    currentGame = new GameData(txtName.Text);
                    LoadScreen(playing);
                }
            };
            uiElements.Add(btnStartGame);

            Button btnGoToMainMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 380, 280, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnGoToMainMenu.onClick = () => { LoadScreen(main); };
            uiElements.Add(btnGoToMainMenu);
        };
        public static Action loadGame = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 20), "Charger une partie", UI_Element.ECenterMode.middle));

            ItemList lstGames = new ItemList(mainSpriteBatch, new Rectangle(halfScreenWidth - 140, 50, 280, 230));

            savePath = Environment.CurrentDirectory + "\\" + GameData.SAVE_DIRECTORY;
            System.IO.Directory.CreateDirectory(savePath);
            string[] saveFiles = System.IO.Directory.GetFiles(savePath, "*.txt");
            foreach (string file in GetSaveFiles()) {
                lstGames.AddItem(file);
            }
            uiElements.Add(lstGames);

            Button btnLoadGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 320, 250, 50), "Charger cette partie", UI_Element.ECenterMode.middle);
            btnLoadGame.onClick = () => {
                if (lstGames.SelectedItem == null) return;
                currentGame = new GameData("");
                currentGame.Load(lstGames.SelectedItem.ToString());
                currentGame.isSoloGame = true;
                LoadScreen(playing);
            };
            uiElements.Add(btnLoadGame);

            Button btnQuitGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 390, 300, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnQuitGame.onClick = () => { LoadScreen(main); };
            uiElements.Add(btnQuitGame);
        };
        public static Action playing = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(screenWidth - 20, 20), "Vous êtes: " + currentGame.IncarnatedPlayer, UI_Element.ECenterMode.right));
            uiElements.Add(new Label(mainSpriteBatch, new Point(screenWidth - 20, 50), "Vos opposants sont: " + string.Join(", ", currentGame.GetOpponents()), UI_Element.ECenterMode.right));

            Label lblCurrentPlayer = new Label(mainSpriteBatch, new Point(20, 80), "C'est à " + currentGame.actualPlayer + " de jouer");
            uiElements.Add(lblCurrentPlayer);

            Button btnPause = new Button(mainSpriteBatch, new Rectangle(20, 20, 150, 50), "Menu pause");
            btnPause.onClick = () => { LoadScreen(pause); };
            uiElements.Add(btnPause);

            GameBoard gameBoard = new GameBoard(mainSpriteBatch, new Point(halfScreenWidth, 100), 350, currentGame, UI_Element.ECenterMode.middle);
            gameBoard.onGridChange = () => {
                lblCurrentPlayer.Text = "C'est à " + currentGame.actualPlayer + " de jouer";
            };
            uiElements.Add(gameBoard);
        };
        public static Action netError = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight - 50), "Une erreur est survenue", UI_Element.ECenterMode.middle));
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight), "Raison: " + Network.errorReason, UI_Element.ECenterMode.middle));

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight + 50, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => {
                if (Network.netServerHandler != null) Network.netServerHandler.Shutdown();
                LoadScreen(main);
            };
            uiElements.Add(btnBackToMenu);
        };
        public static Action networkEnd = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight - 50), "La partie est terminée.", UI_Element.ECenterMode.middle));
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight), "Raison: " + Network.endReason, UI_Element.ECenterMode.middle));

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight + 50, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => {
                if (Network.netServerHandler != null) Network.netServerHandler.Shutdown();
                LoadScreen(main);
            };
            uiElements.Add(btnBackToMenu);
        };
        public static Action final = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight), "Le joueur " + currentGame.lastWinningPlayer + " a gagné !", UI_Element.ECenterMode.middle));

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight + 50, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => {
                if (Network.netServerHandler != null) Network.netServerHandler.Shutdown();
                LoadScreen(main);
            };
            uiElements.Add(btnBackToMenu);
        };
        public static Action tie = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight), "Egalité ! Aucun joueur ne gagne.", UI_Element.ECenterMode.middle));

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight + 50, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => {
                if (Network.netServerHandler != null) Network.netServerHandler.Shutdown();
                LoadScreen(main);
            };
            uiElements.Add(btnBackToMenu);
        };
        public static Action pause = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 100), "Menu pause", UI_Element.ECenterMode.middle));

            Button btnContinue = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 200, 150, 50), "Continue", UI_Element.ECenterMode.middle);
            btnContinue.onClick = () => { LoadScreen(playing);};
            uiElements.Add(btnContinue);

            Button btnSaveCurrentGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 280, 330, 50), "Sauvegarder la partie en cours", UI_Element.ECenterMode.middle);
            btnSaveCurrentGame.onClick = () => { LoadScreen(save); };
            uiElements.Add(btnSaveCurrentGame);

            Button btnGoToMainScreen = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 360, 280, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnGoToMainScreen.onClick = () => {
                if (!currentGame.isSoloGame) {
                    if (Network.isHosting) {
                        Network.netServerHandler.Net_StopGame("Game has been terminated");
                        Network.netServerHandler.Shutdown();
                    }
                }
                LoadScreen(main);
            };
            uiElements.Add(btnGoToMainScreen);
        };
        public static Action save = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 100), "Sauvegarder la partie", UI_Element.ECenterMode.middle));

            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 150), "Entrez le nom de la sauvegarde :", UI_Element.ECenterMode.middle));

            Textbox txtSaveName = new Textbox(mainSpriteBatch, new Rectangle(halfScreenWidth, 180, 150, 40), "", UI_Element.ECenterMode.middle);
            uiElements.Add(txtSaveName);

            Button btnSaveGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 300, 200, 50), "Sauvegarder", UI_Element.ECenterMode.middle);
            btnSaveGame.onClick = () => {
                if (txtSaveName.Text.Length > 0) {
                    //Save current game
                    currentGame.Save(txtSaveName.Text);
                    LoadScreen(confirmSave);
                }
            };
            uiElements.Add(btnSaveGame);

            Button btnGoBack = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 380, 150, 50), "Retour", UI_Element.ECenterMode.middle);
            btnGoBack.onClick = () => { LoadScreen(pause); };
            uiElements.Add(btnGoBack);
        };
        public static Action confirmSave = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 100), "Partie sauvegardée", UI_Element.ECenterMode.middle));

            Button btngoBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, 300, 250, 50), "Retour au menu pause", UI_Element.ECenterMode.middle);
            btngoBackToMenu.onClick = () => { LoadScreen(pause);};
            uiElements.Add(btngoBackToMenu);
        };
        public static Action waitForGameToStart = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight - 80), "En attente du démarrage de la partie", UI_Element.ECenterMode.middle));
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight), "Nom de l'hôte:" + Network.netClientHandler.serverHostName, UI_Element.ECenterMode.middle));

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight + 50, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => {
                Network.netClientHandler.Shutdown();
                LoadScreen(main);
            };
            uiElements.Add(btnBackToMenu);
        };
        public static Action waitForPlayerToJoin = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight), "En attente de la connexion de joueurs", UI_Element.ECenterMode.middle));

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight + 50, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => {
                Network.netServerHandler.Shutdown();
                LoadScreen(main);
            };
            uiElements.Add(btnBackToMenu);
        };
        public static Action tryToConnectToServer = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 80), "Tentative de connexion à l'hôte", UI_Element.ECenterMode.middle));
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 150), "Nom de l'hôte:" + Network.netClientHandler.serverHostName, UI_Element.ECenterMode.middle));
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight + 50), "Veuillez patienter", UI_Element.ECenterMode.middle));

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, screenHeight - 100, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => {
                Network.netClientHandler.Shutdown();
                LoadScreen(main);
            };
            uiElements.Add(btnBackToMenu);
        };
        public static Action joinOnlineGame = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 30), "Rejoindre une partie en ligne", UI_Element.ECenterMode.middle));

            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight - 130), "Entrez le nom DNS / IP du server", UI_Element.ECenterMode.middle));
            Textbox txtServerHostName = new Textbox(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight - 100, 150, 40), "127.0.0.1", UI_Element.ECenterMode.middle);
            uiElements.Add(txtServerHostName);

            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight - 30), "Entrez votre nom", UI_Element.ECenterMode.middle));
            Textbox txtPlayerName = new Textbox(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight, 150, 40), "", UI_Element.ECenterMode.middle);
            uiElements.Add(txtPlayerName);


            Button btnJoinGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight + 50, 220, 50), "Rejoindre une partie", UI_Element.ECenterMode.middle);
            btnJoinGame.onClick = () => {
                if (txtServerHostName.Text == "" || txtPlayerName.Text == "") return;

                try {
                    if (Network.netClientHandler != null) { Network.netClientHandler.Shutdown(); }
                    Network.netClientHandler = new NetworkHandler_Client(txtServerHostName.Text, txtPlayerName.Text);
                } catch (Lidgren.Network.NetException netException) {
                    Network.errorReason = netException.Message;
                    Network.netClientHandler.Shutdown();
                    LoadScreen(netError);
                    return;
                }
                LoadScreen(tryToConnectToServer);
            };
            uiElements.Add(btnJoinGame);

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, screenHeight - 100, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => { LoadScreen(main); };
            uiElements.Add(btnBackToMenu);
        };
        public static Action hostOnlineGame = () => {
            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, 30), "Héberger une partie en ligne", UI_Element.ECenterMode.middle));

            uiElements.Add(new Label(mainSpriteBatch, new Point(halfScreenWidth, halfScreenHeight - 30), "Entrez votre nom", UI_Element.ECenterMode.middle));
            Textbox txtPlayerName = new Textbox(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight, 150, 40), "", UI_Element.ECenterMode.middle);
            uiElements.Add(txtPlayerName);

            Button btnHostGame = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, halfScreenHeight + 50, 220, 50), "Héberger une partie", UI_Element.ECenterMode.middle);
            btnHostGame.onClick = () => {
                if (Network.netServerHandler != null) { Network.netServerHandler.Shutdown(); }
                try {
                    Network.netServerHandler = new NetworkHandler_Server(new GameData());
                } catch (System.Net.Sockets.SocketException) {
                    Network.errorReason = "Impossible d'avoir plusieurs serveurs sur le même port";
                    LoadScreen(netError);
                    return;
                }
                Network.netServerHandler.playersList.Add(txtPlayerName.Text);
                LoadScreen(waitForPlayerToJoin);
            };
            uiElements.Add(btnHostGame);

            Button btnBackToMenu = new Button(mainSpriteBatch, new Rectangle(halfScreenWidth, screenHeight - 100, 250, 50), "Retour au menu principal", UI_Element.ECenterMode.middle);
            btnBackToMenu.onClick = () => { LoadScreen(main); };
            uiElements.Add(btnBackToMenu);
        };
        public static Action debug = () => {
            Button btnGetData = new Button(mainSpriteBatch, new Rectangle(400, 300, 200, 50), "Get data", UI_Element.ECenterMode.middle);
            btnGetData.onClick = () => {
                var currentItem = uiElements.OfType<ItemList>().First().SelectedItem;
                var currenText = uiElements.OfType<Textbox>().First().Text;
                Console.WriteLine("Current selected item is \"{0}\" and the textbox is displaying \"{1}\"", currentItem, currenText);
            };
            uiElements.Add(btnGetData);


            Button btnAddItem = new Button(mainSpriteBatch, new Rectangle(400, 360, 200, 50), "Add item", UI_Element.ECenterMode.middle);
            btnAddItem.onClick = () => {
                uiElements.OfType<ItemList>().First().AddItem(new GameData(DateTime.Now.ToString()));
            };
            uiElements.Add(btnAddItem);

            Button btnRemoveItem = new Button(mainSpriteBatch, new Rectangle(400, 420, 200, 50), "Remove item", UI_Element.ECenterMode.middle);
            btnRemoveItem.onClick = () => {
                ItemList itemList = uiElements.OfType<ItemList>().First();
                if (itemList.Items.Count > 0) {
                    itemList.RemoveItemAt(itemList.Items.Count - 1);
                }
            };
            uiElements.Add(btnRemoveItem);

            uiElements.Add(new Label(mainSpriteBatch, new Point(400, 200), "This is a label", UI_Element.ECenterMode.middle));

            uiElements.Add(new Textbox(mainSpriteBatch, new Rectangle(400, 100, 200, 35), "This is a textbox"));

            uiElements.Add(new ItemList(mainSpriteBatch, new Rectangle(50, 100, 200, 280),
                new List<object> {
                    new GameData("test 1"),
                    new GameData("test 2"),
                    new GameData("test 3"),
                    new GameData("test 4"),
                    new GameData("test 5"),
                    new GameData("test 6"),
                    new GameData("test 7")
                }
            ));
        };
        #endregion
    }
}
